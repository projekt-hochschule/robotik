
package handhabungsaufgabe;


import static com.kuka.roboticsAPI.motionModel.BasicMotions.circ;


import static com.kuka.roboticsAPI.motionModel.BasicMotions.batch;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.lin;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.linRel;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.positionHold;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.ptp;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.spl; //neu damit Splinebewegungen durchgef�hrt werden k�nnen



import handhabungsaufgabe.PositionAndGMSReferencing;

import java.util.concurrent.TimeUnit;


import com.kuka.roboticsAPI.RoboticsAPIContext;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;

import com.kuka.roboticsAPI.controllerModel.Controller;

import com.kuka.roboticsAPI.deviceModel.LBR;

import com.kuka.roboticsAPI.geometricModel.CartDOF;
import com.kuka.roboticsAPI.geometricModel.Tool;
import com.kuka.roboticsAPI.geometricModel.math.Transformation;

import com.kuka.roboticsAPI.motionModel.MotionBatch;
import com.kuka.roboticsAPI.motionModel.Spline;
import com.kuka.roboticsAPI.motionModel.controlModeModel.CartesianImpedanceControlMode;

import com.kuka.roboticsAPI.sensorModel.DataRecorder;
import com.kuka.roboticsAPI.uiModel.ApplicationDialogType;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKey;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKeyBar;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKeyListener;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyAlignment;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyEvent;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyLED;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyLEDSize;
import com.kuka.common.ThreadUtil;
import com.kuka.generated.ioAccess.IOModulIOGroup;
import com.kuka.generated.ioAccess.MediaFlangeIOGroup;
import static com.kuka.roboticsAPI.motionModel.HRCMotions.*;


/**
 * Implementation of a robot application.
 * <p>
 * The application provides a {@link RoboticsAPITask#initialize()} and a 
 * {@link RoboticsAPITask#run()} method, which will be called successively in 
 * the application lifecycle. The application will terminate automatically after 
 * the {@link RoboticsAPITask#run()} method has finished or after stopping the 
 * task. The {@link RoboticsAPITask#dispose()} method will be called, even if an 
 * exception is thrown during initialization or run. 
 * <p>
 * <b>It is imperative to call <code>super.dispose()</code> when overriding the 
 * {@link RoboticsAPITask#dispose()} method.</b> 
 * 
 * @see UseRoboticsAPIContext
 * @see #initialize()
 * @see #run()
 * @see #dispose()
 */
public class ProgrammierungHandhabungsaufgabe extends RoboticsAPIApplication {
	public Controller kuka_Sunrise_Cabinet_1;
	public LBR Roboter;
	public IOModulIOGroup IOModul;
	public MediaFlangeIOGroup MediaFlangeIOGroup;
	public Werkzeug wkz; //Werkzeugklasse f�r Programmieraufgabe
	public Tool Fingergreifer;
	public Tool Transporter;
	public CartesianImpedanceControlMode cartImpCtrlMode;
	public Spline SPBsp;

	
	// Hier werden die Objekte initialisiert
	@Override
	public void initialize() {
		kuka_Sunrise_Cabinet_1 = getController("KUKA_Sunrise_Cabinet_1");
		Roboter = (LBR) getDevice(kuka_Sunrise_Cabinet_1,
				"LBR_iiwa_7_R800_1");
		IOModul = new IOModulIOGroup(kuka_Sunrise_Cabinet_1);
		MediaFlangeIOGroup = new MediaFlangeIOGroup(kuka_Sunrise_Cabinet_1);
		Fingergreifer = getApplicationData().createFromTemplate("Fingergreifer");
		Transporter = getApplicationData().createFromTemplate("Transporter");
		wkz = new Werkzeug(this);
		setImpedanz();
		
		if(!Roboter.getSafetyState().areAllAxesPositionReferenced() || !Roboter.getSafetyState().areAllAxesGMSReferenced()){
			getApplicationUI().displayModalDialog(ApplicationDialogType.INFORMATION, "Referenzierung wird durchgef�hrt.", "OK");
			PositionAndGMSReferencing ref = new PositionAndGMSReferencing(this);
			ref.startRef();
		}
	}
	
	//Impedanzregler; Ver�ndern Sie diese Werte nicht!
	public void setImpedanz() {
	
		cartImpCtrlMode = new CartesianImpedanceControlMode();
		
		cartImpCtrlMode.parametrize(CartDOF.A).setStiffness(200);						//Federsteifigkeit f�r rotatorischen Freiheitsgrad in z-Achse [Nm/rad]
		cartImpCtrlMode.parametrize(CartDOF.B).setStiffness(200);						//Federsteifigkeit f�r rotatorischen Freiheitsgrad in y-Achse [Nm/rad]
		cartImpCtrlMode.parametrize(CartDOF.C).setStiffness(200);						//Federsteifigkeit f�r rotatorischen Freiheitsgrad in x-Achse [Nm/rad]
		
		cartImpCtrlMode.parametrize(CartDOF.X).setStiffness(1000);						//Federsteifigkeit f�r translatorischen Freiheitsgrad in x-Achse [N/m]			
		cartImpCtrlMode.parametrize(CartDOF.Y).setStiffness(1000);						//Federsteifigkeit f�r translatorischen Freiheitsgrad in y-Achse [N/m]		 	
		cartImpCtrlMode.parametrize(CartDOF.Z).setStiffness(500);						//Federsteifigkeit f�r translatorischen Freiheitsgrad in z-Achse [N/m]	
		
		cartImpCtrlMode.parametrize(CartDOF.ALL).setDamping(0.3);						//Federd�mpfung f�r alle Freiheitsgrade

		cartImpCtrlMode.setMaxControlForce(150.0, 150.0, 150.0, 15.0, 15.0, 15.0, true);	//max. Kraft am TCP [N] und [Nm]
		cartImpCtrlMode.setMaxPathDeviation(4, 4, 4, 0.03, 0.03, 0.03);						//max. Bahnabweichung [mm] und [rad]
		
		cartImpCtrlMode.setNullSpaceStiffness(100);										//Federsteifigkeit des redundanten Freiheitsgrad
		cartImpCtrlMode.setNullSpaceDamping(0.3);										//Federd�mpfung des redundanten Freiheitsgrad
	}


	/**In public void run() erfolgt die Programmierung der Handhabungsaufgabe
	 */
	//In public void run() programmieren Sie alle ihre Bewegegungsabl�ufe, aktivieren/deaktiviern alle erforderlichen Ventile und werten Sensorinformationen aus
	@Override
	public void run() 
	{
       
		/*Roboter.move(ptp(getApplicationData().getFrame("/VTBasisKS/Home1")).setJointVelocityRel(0.2));  //Ruhestellung
		wkz.WerkzeugHolen(Transporter);
		while(IOModul.getS2()){
			getApplicationUI().displayModalDialog(ApplicationDialogType.INFORMATION, "Palettenbeladungstation belegt. Progamm kann nicht fortgef�hrt werden.", "OK");
		} 
		*/
		
				Roboter.move(ptp(getApplicationData().getFrame("/VTBasisKS/Home1")).setJointVelocityRel(0.3));
		
		//////////////////////////////////////////////HIER ANFANGEN ZU PROGRAMMIEREN//////////////////////////////////////////////////////////
		
				wkz.WerkzeugHolen(Fingergreifer); //Werkzeug abholen
				
				Spline myspline = new Spline(lin(getApplicationData().getFrame("/P1")).setJointVelocityRel(0.3),
											 spl(getApplicationData().getFrame("/P2")).setJointVelocityRel(0.2), 
											 spl(getApplicationData().getFrame("/P3")).setJointVelocityRel(0.1));
				Roboter.move(myspline);						
				Roboter.move(ptp(getApplicationData().getFrame("/P4")).setJointVelocityRel(0.02));
				fingerZustand(1);
				
				
				//Zuerueck zu p3 und pallette bewegen
				pallettebewegen(3);
				
				//Ueberpruefen, ob die moeglichsche Stellplatz freie ist
					boolean palette_1_legen = false;
					boolean palette_2_legen = false;
					boolean palette_3_legen = false;
					boolean palette_4_legen = false;
					boolean palette_1_schon_aufgelegt = false;
					boolean palette_2_schon_aufgelegt = false;
					boolean palette_3_schon_aufgelegt = false;
					boolean palette_4_schon_aufgelegt = false;
					
					
				for(int i=0; i < 4; i++)
				{
					////////////////////////////////PALETTE (egal welsche Reihenfolge auflegen)////////////////////////////////
					 
					sensorfehler(); //Fehlermeldung, wenn die erforderliche Sensore nicht beruecksichtig werden sensore 
					
					if(!sensorenZustand(6) && sensorenZustand(5) && sensorenZustand(7) && sensorenZustand(4))
					{
						palette_4_legen = true;
						
						pallette_auflegen(0,0,0, 1, palette_4_legen,palette_4_schon_aufgelegt);
						
						palette_4_schon_aufgelegt = true;

						fingerZustand(2); // Palette auflegen
						Roboter.move(linRel(Transformation.ofDeg(0, 0, 20, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")));
						
						if(i == 0)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P4")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P5")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(4);
						}
						else if(i == 1)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P5")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P6")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(5);
						}
						else if(i == 2)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P6")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P7")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(6);
						}
						else
						{
							break;
						}
						
						continue;
					}
					
					if(!sensorenZustand(4) && sensorenZustand(5) && sensorenZustand(6) && sensorenZustand(7) ) 
					{
						palette_3_legen = true;

						pallette_auflegen(0,0,1, 0, palette_3_legen,palette_3_schon_aufgelegt);
						palette_3_schon_aufgelegt = true;

						fingerZustand(2); // Palette auflegen
						Roboter.move(linRel(Transformation.ofDeg(0, 0, 20, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS"))); 
						
						if(i == 0)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P4")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P5")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(4);
						}
						else if(i == 1)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P5")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P6")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(5);
						}
						else if(i == 2)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P6")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P7")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(6);
						}
						else
						{
							break;
						}
						
						continue;
						
					}
					
					if(!sensorenZustand(5) && sensorenZustand(6) && sensorenZustand(7) && sensorenZustand(4))
				 	{
					palette_2_legen = true;

						pallette_auflegen(0,1,0, 0, palette_2_legen,palette_2_schon_aufgelegt);
						palette_2_schon_aufgelegt = true;

						fingerZustand(2); // Palette auflegen
						Roboter.move(linRel(Transformation.ofDeg(0, 0, 20, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")));  
						
						if(i == 0)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P4")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P5")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(4);
						}
						else if(i == 1)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P5")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P6")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(5);
						}
						else if(i == 2)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P6")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P7")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(6);
						}
						else
						{
							break;
						}
						
						continue;
				 	}
					
					if(!sensorenZustand(7) && sensorenZustand(5) && sensorenZustand(6) && sensorenZustand(4))
				  	{		
						palette_1_legen = true;
						
						pallette_auflegen(1,0,0, 0, palette_1_legen,palette_1_schon_aufgelegt); 
						palette_1_schon_aufgelegt = true;
						
						
						fingerZustand(2); // Palette auflegen
						Roboter.move(linRel(Transformation.ofDeg(0, 0, 20, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")));
						
						if(i == 0)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P4")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P5")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(4);
						}
						else if(i == 1)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P5")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P6")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(5);
						}
						else if(i == 2)
						{
							Roboter.move(ptp(getApplicationData().getFrame("/P6")).setJointVelocityRel(0.2));
							Roboter.move(ptp(getApplicationData().getFrame("/P7")).setJointVelocityRel(0.02));
							fingerZustand(1);
							pallettebewegen(6);
						}
						else
						{
							break;
						}
						
						continue;
				  	}
				}
				
				Roboter.move(ptp(getApplicationData().getFrame("/P11")).setJointVelocityRel(0.2));	
				
				
//////////////////////////////////////////////////HOERSCHEN IN DER PALETTE AUFLEGEN///////////////////////////////////////////////////////////////////////
		
	int x = 0;
	while(x < 4) 
	{
		int y=0;
		while(y < 2) 
		{
			fingerZustand(1);
			
			Spline myspline1 = new Spline(linRel(Transformation.ofDeg(0, 0, 0.01, -0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2),
					 spl(getApplicationData().getFrame("/P_referenz_punkt_1_roh_holen")).setJointVelocityRel(0.2), 
					 linRel(Transformation.ofDeg(-42*x, 40*y, 0, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			Roboter.move(myspline1);
			
			
			Roboter.move(linRel(Transformation.ofDeg(0, 0, -67, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.1));
			
			
			fingerZustand(2);
			
			Roboter.move(linRel(Transformation.ofDeg(0, 0,60, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			
			//Bewegung roh ablegen
			if(x == 0 || x == 1)
			{
				Roboter.move(ptp(getApplicationData().getFrame("/P_zw_punkt_roh_ablegen_1")).setJointVelocityRel(0.2));
				Roboter.move(linRel(Transformation.ofDeg(-21*x, 20*y, 0, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
				Roboter.move(linRel(Transformation.ofDeg(0, 0, -55, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.08));
				
				fingerZustand(1);
				
				//Roboter.move(linRel(Transformation.ofDeg(0, 0,50, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			}
			
			if(x == 2 || x == 3)
			{
				Roboter.move(ptp(getApplicationData().getFrame("/P_zw_punkt_roh_ablegen_2")).setJointVelocityRel(0.2));
				Roboter.move(linRel(Transformation.ofDeg(-21*(x-2), 20*y, 0, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
				Roboter.move(linRel(Transformation.ofDeg(0, 0, -55, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.08));
				
				fingerZustand(1);
			}
			
			Roboter.move(linRel(Transformation.ofDeg(0, 0,50, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			
			y++;
		}
		x++;
	}
	
	x = 0;
	while(x < 4) 
	{
		int y=0;
		while(y < 2) 
		{
			fingerZustand(1);
			Spline myspline1 = new Spline(linRel(Transformation.ofDeg(0, 0, 0.01, -0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2),
					 spl(getApplicationData().getFrame("/P_referenz_punkt_2_roh_holen")).setJointVelocityRel(0.2), 
					 linRel(Transformation.ofDeg(42*x, 40*y, 0, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			Roboter.move(myspline1);
			
			Roboter.move(linRel(Transformation.ofDeg(0, 0, -54, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.1));
			
			
			
			fingerZustand(2);
			
			Roboter.move(linRel(Transformation.ofDeg(0, 0,60, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			
			//Bewegung roh ablegen
			if(x == 0 || x == 1)
			{
				Roboter.move(ptp(getApplicationData().getFrame("/P_zw_punkt_roh_ablegen_3")).setJointVelocityRel(0.2));
				Roboter.move(linRel(Transformation.ofDeg(-21*x, 20*y, 0, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
				Roboter.move(linRel(Transformation.ofDeg(0, 0, -55, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.08));
				
				fingerZustand(1);
				
				
			}
			
			if(x == 2 || x == 3)
			{
				Roboter.move(ptp(getApplicationData().getFrame("/P_zw_punkt_roh_ablegen_4")).setJointVelocityRel(0.2));
				Roboter.move(linRel(Transformation.ofDeg(-21*(x-2), 20*y, 0, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
				Roboter.move(linRel(Transformation.ofDeg(0, 0, -55, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.08));
				
				fingerZustand(1);
			}
			
			
			Roboter.move(linRel(Transformation.ofDeg(0, 0, 50, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			y++;
		}
		x++;
	}
	fingerZustand(2);
				
	
/////////////////////////////////////////////////////////Komplette Palette bewegen/////////////////////////////////////////////////////////////////////////

	//Fingergreifer ablegen und Transporter abholen

	wkz.WerkzeugAblegen();
	wkz.WerkzeugHolen(Transporter);
	Roboter.move(ptp(getApplicationData().getFrame("/P_zw_punkt_45_grad")).setJointVelocityRel(0.2));
	Roboter.move(ptp(getApplicationData().getFrame("/P_zw_punkt_45_grad_runter")).setJointVelocityRel(0.02));
	Roboter.move(ptp(getApplicationData().getFrame("/P_zw_punkt_minus_45_grad")).setJointVelocityRel(0.2));

	Roboter.move(ptp(getApplicationData().getFrame("/P_palette_holen_1")).setJointVelocityRel(0.02));
	Roboter.move(ptp(getApplicationData().getFrame("/P_palette_holen_2")).setJointVelocityRel(0.2));
	Roboter.move(ptp(getApplicationData().getFrame("/P_palette_holen_3_fixiert")).setJointVelocityRel(0.02));

	Roboter.move(ptp(getApplicationData().getFrame("/P_palette_oben_1")).setJointVelocityRel(0.02));
	Roboter.move(ptp(getApplicationData().getFrame("/P_palette_oben_2")).setJointVelocityRel(0.2));
	Roboter.move(ptp(getApplicationData().getFrame("/P_palette_aufgelegt_1")).setJointVelocityRel(0.02));
	Roboter.move(ptp(getApplicationData().getFrame("/P_palette_aufgelegt_2")).setJointVelocityRel(0.2));
	Roboter.move(ptp(getApplicationData().getFrame("/P_palette_aufgelegt_raus")).setJointVelocityRel(0.02));	
	wkz.WerkzeugAblegen();

	}

	private void pallette_auflegen(int a, int b, int c, int d, boolean palette_legen, boolean schon_aufgelegt)
	{
		
		if(a == 1 && b == 0 && c == 0 && d == 0)
		{
			//palette_legen = true;
			
			if (palette_legen == true && schon_aufgelegt == true)
			
			{
					//Rotation Z-Richtung durchfuehren
					Roboter.move(linRel(Transformation.ofDeg(0, 0, 10, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")));
					Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 0, -180, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.3),
							 spl(getApplicationData().getFrame("/P11_revers")).setJointVelocityRel(0.2), 
							 spl(getApplicationData().getFrame("/P14_1_w_left")).setJointVelocityRel(0.1));
					Roboter.move(myspline);
					
					Roboter.move(ptp(getApplicationData().getFrame("/P14_w_left")).setJointVelocityRel(0.02));	
			}

			else if(palette_legen==true)
			{
				Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 10, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.3),
						 spl(getApplicationData().getFrame("/P11")).setJointVelocityRel(0.2), 
						 spl(getApplicationData().getFrame("/P13_1_red_left")).setJointVelocityRel(0.1));
				Roboter.move(myspline);
				Roboter.move(ptp(getApplicationData().getFrame("/P13_red_left")).setJointVelocityRel(0.02));
				schon_aufgelegt = true;	
			}
		}
		
		if(a == 0 && b == 1 && c == 0 && d == 0)
		{
			if(palette_legen == true && schon_aufgelegt == true)
			{
				//Rotation Z-Richtung durchfuehren
				Roboter.move(linRel(Transformation.ofDeg(0, 0, 10, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")));
				Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 0, -180, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.3),
						 spl(getApplicationData().getFrame("/P11_revers")).setJointVelocityRel(0.2), 
						 spl(getApplicationData().getFrame("/P13_1_w_left")).setJointVelocityRel(0.1));
				Roboter.move(myspline);
				Roboter.move(ptp(getApplicationData().getFrame("/P13_w_left")).setJointVelocityRel(0.02));
			}
			
			else if(palette_legen == true )
			{
				Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 10, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.3),
						 spl(getApplicationData().getFrame("/P11")).setJointVelocityRel(0.2), 
						 spl(getApplicationData().getFrame("/P14_1_red_left")).setJointVelocityRel(0.1));
				Roboter.move(myspline);
				Roboter.move(ptp(getApplicationData().getFrame("/P14_red_left")).setJointVelocityRel(0.02));
				schon_aufgelegt = true;
			}
			
		}
		
		if(a == 0 && b == 0 && c == 1 && d == 0)
		{			
			if(palette_legen == true && schon_aufgelegt == true)
			{
				//Rotation Z-Richtung durchfuehren
				Roboter.move(linRel(Transformation.ofDeg(0, 0, 10, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")));
				Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 0, -180, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.3),
						 spl(getApplicationData().getFrame("/P11_revers")).setJointVelocityRel(0.2), 
						 spl(getApplicationData().getFrame("/P15_1_w_left")).setJointVelocityRel(0.1));
				Roboter.move(myspline);
				
				
				Roboter.move(ptp(getApplicationData().getFrame("/P15_w_left")).setJointVelocityRel(0.02));
			}
			
			else if(palette_legen==true)
			{
				Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 10, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.3),
						 spl(getApplicationData().getFrame("/P11")).setJointVelocityRel(0.2), 
						 spl(getApplicationData().getFrame("/P16_1_red_left")).setJointVelocityRel(0.1));
				Roboter.move(myspline);
				Roboter.move(ptp(getApplicationData().getFrame("/P16_red_left")).setJointVelocityRel(0.02));
				schon_aufgelegt = true;
			}
			
		}
		
		if(a == 0 && b == 0 && c == 0 && d == 1)
		{
			if(palette_legen == true && schon_aufgelegt == true)
			{
				//Rotation Z-Richtung durchfuehren
				Roboter.move(linRel(Transformation.ofDeg(0, 0, 10, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")));
				Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 0, -180, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.3),
						 spl(getApplicationData().getFrame("/P11_revers")).setJointVelocityRel(0.2), 
						 spl(getApplicationData().getFrame("/P16_1_w_left")).setJointVelocityRel(0.1));
				Roboter.move(myspline);
				
				Roboter.move(ptp(getApplicationData().getFrame("/P16_w_left")).setJointVelocityRel(0.02));
			}
			
			else if(palette_legen==true && schon_aufgelegt == false)
			{
				Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 10, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.3),
						 spl(getApplicationData().getFrame("/P11")).setJointVelocityRel(0.2), 
						 spl(getApplicationData().getFrame("/P15_1_red_left")).setJointVelocityRel(0.1));
				Roboter.move(myspline);	
				Roboter.move(ptp(getApplicationData().getFrame("/P15_red_left")).setJointVelocityRel(0.02));
				schon_aufgelegt = true;
			}
			 
		}
	}
	
	
	
	private boolean sensorenZustand(int input)
	{
		boolean inputValue = false; //Standardmaessig Schalter auf false

		//input wird ausgewertet und case durchgegangen
		switch(input)
		{
			case 1:
				inputValue = IOModul.getS1();
				break;
			case 2:
				inputValue = IOModul.getS2();
				break;
			case 3:
				inputValue = IOModul.getS3();
				break;
			case 4:
				inputValue = IOModul.getS4();
				break;
			case 5:
				inputValue = IOModul.getS5();
				break;
			case 6:
				inputValue = IOModul.getS6();
				break;
			case 7:
				inputValue = IOModul.getS7();
				break;
			default:
				inputValue = false;
				break;
		}

		return inputValue;
	}
	
	public void pallettebewegen(int i)
	{
		if(i == 3)
		{
			Roboter.move(linRel(Transformation.ofDeg(0, 0, 15, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 0, -90, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.4),
										 spl(getApplicationData().getFrame("/P10")).setJointVelocityRel(0.3), 
										 spl(getApplicationData().getFrame("/P9")).setJointVelocityRel(0.1));
			Roboter.move(myspline);	
			Roboter.move(ptp(getApplicationData().getFrame("/P8")).setJointVelocityRel(0.02));
		}
		else if(i == 4)
		{
			Roboter.move(linRel(Transformation.ofDeg(0, 0, 15, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 0, -90, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.4),
										 spl(getApplicationData().getFrame("/P10")).setJointVelocityRel(0.3), 
										 spl(getApplicationData().getFrame("/P9")).setJointVelocityRel(0.1));
			Roboter.move(myspline);	
			Roboter.move(ptp(getApplicationData().getFrame("/P8")).setJointVelocityRel(0.02));
		}
		else if(i == 5)
		{
			Roboter.move(linRel(Transformation.ofDeg(0, 0, 15, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 0, -90, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.4),
										 spl(getApplicationData().getFrame("/P10")).setJointVelocityRel(0.3), 
										 spl(getApplicationData().getFrame("/P9")).setJointVelocityRel(0.1));
			Roboter.move(myspline);	
			Roboter.move(ptp(getApplicationData().getFrame("/P8")).setJointVelocityRel(0.02));
		}
		else if(i == 6)
		{
			Roboter.move(linRel(Transformation.ofDeg(0, 0, 15, 0, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.2));
			Spline myspline = new Spline(linRel(Transformation.ofDeg(0, 0, 0, -90, 0, 0),getApplicationData().getFrame("/VTBasisKS")).setJointVelocityRel(0.4),
										 spl(getApplicationData().getFrame("/P10")).setJointVelocityRel(0.3), 
										 spl(getApplicationData().getFrame("/P9")).setJointVelocityRel(0.1));
			Roboter.move(myspline);	
			Roboter.move(ptp(getApplicationData().getFrame("/P8")).setJointVelocityRel(0.02));
		}
	}
	
	public void sensorfehler()
	{
		if((!sensorenZustand(5) && !sensorenZustand(6)) || (!sensorenZustand(5) && !sensorenZustand(7)) || (!sensorenZustand(5) && !sensorenZustand(4))||
				  (!sensorenZustand(6) && !sensorenZustand(7)) || (!sensorenZustand(6) && !sensorenZustand(4)) || (!sensorenZustand(7) && !sensorenZustand(4)))
			{
				//getApplicationUI().displayModalDialog(ApplicationDialogType.INFORMATION, "Programm beendet. Bitte den Schalter umlegen.", "OK");
				fingerZustand(1);
				Roboter.move(ptp(getApplicationData().getFrame("/P9")).setJointVelocityRel(0.2));
				Roboter.move(ptp(getApplicationData().getFrame("/P8")).setJointVelocityRel(0.02));
				fingerZustand(2);
				fingerZustand(1);
				getApplicationUI().displayModalDialog(ApplicationDialogType.INFORMATION, "Die ben�tige Sensoren wurden leider nicht komplett aktiviert. �berpr�fen Sie ob die aktiv sind und Klicken Sie auf \"OK\", um mit dem Progamm forzufahren. Gegebenefalls brechen Sie das Programm ab.","OK" );
			}
	}
	
	private void fingerZustand(int input)
	{
		switch(input)
		{
		case 1:
			//Fingergreifer auf
			MediaFlangeIOGroup.setOutput2(true); 
			ThreadUtil.milliSleep(200);
			MediaFlangeIOGroup.setOutput1(false); 
			ThreadUtil.milliSleep(1000);
			break;
		case 2:
			//Fingergreifer zu
			MediaFlangeIOGroup.setOutput1(true); 
			ThreadUtil.milliSleep(200);
			MediaFlangeIOGroup.setOutput2(false); 
			ThreadUtil.milliSleep(1000);
			break;
		}
	}
	
		
	/**
	 * Auto-generated method stub. Do not modify the contents of this method.
	 */
	public static void main(String[] args) {
		ProgrammierungHandhabungsaufgabe app = new ProgrammierungHandhabungsaufgabe();
		app.runApplication();
	}
}
